module Dependency.Analyser
( search
, follow
, SearchType(..)
) where

import Data.Map as Map
import qualified Data.List as List
import Data.Maybe as Maybe
import Data.List.Split as Split
import Data.Set as Set
import Data.Tree as Tree

import Dependency.Parser

data GAV = GAV { group :: String
               , artifact :: String
               , version :: String
} deriving (Show, Eq)

data SearchType = Group
                | Artifact
                | FreeText
                | Version
                deriving (Eq, Show)

type MatcherFunction = String -> Set String -> String -> Bool

keyMatcher :: MatcherFunction
keyMatcher key _ term = key == term

subStringMatcher :: MatcherFunction
subStringMatcher k _ term = List.isInfixOf term k

extractGav :: DependencyRep -> Maybe GAV
extractGav dep =
            let parts = Split.splitOn ":" dep
            in case parts of
                (g:a:v:[]) -> Just $ GAV g a v
                _ -> Nothing

assocWhichMatch :: DependencyAssoc -> DependencyRep -> MatcherFunction -> DependencyAssoc
assocWhichMatch assoc dep finder = Map.filterWithKey (\k v -> finder k v dep) assoc

search :: SearchType -> DependencyAssoc -> String -> [DependencyRep]
search Group assoc dep = extractKeys $ assocWhichMatch assoc dep (\key _ term -> Just term == (fmap group $ extractGav key))
search Artifact assoc dep = extractKeys $ assocWhichMatch assoc dep (\key _ term -> Just term == (fmap artifact $ extractGav key))
search Version assoc dep = extractKeys $ assocWhichMatch assoc dep (\key _ term -> Just term == (fmap version $ extractGav key))
search FreeText assoc dep = extractKeys $ assocWhichMatch assoc dep subStringMatcher

extractKeys :: DependencyAssoc -> [DependencyRep]
extractKeys assoc = Map.foldWithKey (\k _ acc -> k : acc) [] assoc

follow :: DependencyParentAssoc -> DependencyRep -> Tree DependencyRep
follow assoc dep =
            case Map.lookup dep assoc of
                Nothing -> Node dep []
                Just parents -> if parents == Set.empty then Node dep [] else
                    Node dep $ List.map (\p -> follow assoc p) $ Set.toList parents