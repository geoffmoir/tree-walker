module Dependency.Parser
( childMapping
, parentMapping
, DependencyRep
, DependencyChildAssoc
, DependencyParentAssoc
, DependencyAssoc
) where

import Data.Char as Char
import Data.Map as Map
import Data.Set as Set
import Data.Maybe as Maybe

type DependencyRep = String
type DependencyChildAssoc = Map DependencyRep (Set DependencyRep)
type DependencyParentAssoc = DependencyChildAssoc
type DependencyAssoc = DependencyChildAssoc

nestedLevel :: DependencyRep -> Int
nestedLevel s = nestedLevelInternal s 0

nestedLevelInternal :: DependencyRep -> Int -> Int
nestedLevelInternal [] n = n
nestedLevelInternal (x:xs) n
        | x `elem` ['│', '├', '└'] = nestedLevelInternal xs (n+1)
        | ignoredChars x = nestedLevelInternal xs n
        | otherwise = n
        where ignoredChars c = Char.isSpace c || c `elem` ['─']

stripDependency :: DependencyRep -> DependencyRep
stripDependency [] = []
stripDependency dep @ (x:xs)
        | Char.isAlphaNum x = dep
        | otherwise = stripDependency xs

isLastDependencyInList :: DependencyRep -> Bool
isLastDependencyInList (x:xs)
        | x == '└' = True
        | Char.isAlphaNum x = False
        | otherwise = isLastDependencyInList(xs)

dependenciesForHead :: [DependencyRep] -> Set DependencyRep
dependenciesForHead (x:xs) = let childLevel = if (isLastDependencyInList x) then nestedLevel x else nestedLevel x + 1
        in Set.fromList $ dependenciesForHeadInternal xs childLevel []

dependenciesForHeadInternal :: [DependencyRep] -> Int -> [DependencyRep] -> [DependencyRep]
dependenciesForHeadInternal [] _ acc = acc
dependenciesForHeadInternal (x: xs) index acc
        | xs == [] = if depLevel == index then acc ++ [stripDependency x] else acc
        | depLevel < index = acc
        | depLevel > index = dependenciesForHeadInternal xs index acc
        | otherwise = dependenciesForHeadInternal xs index $ acc ++ [stripDependency x]
        where depLevel = nestedLevel x

parentForHead :: [DependencyRep] -> Set DependencyRep
parentForHead [x] = Set.empty
parentForHead (x:y:xs) =
        let isIndented = (isLastDependencyInList x) && (isLastDependencyInList y)
            parentLevel = if (isIndented) then nestedLevel x else nestedLevel x - 1
        in parentForHeadInternal (y:xs) parentLevel

parentForHeadInternal :: [DependencyRep] -> Int -> Set DependencyRep
parentForHeadInternal [] _ = Set.empty
parentForHeadInternal (x:xs) parentIndex
        | depLevel == parentIndex = Set.singleton $ stripDependency x
        | otherwise = parentForHeadInternal xs parentIndex
        where depLevel = nestedLevel x

childMapping :: [DependencyRep] -> DependencyChildAssoc
childMapping file = walkInternal file dependenciesForHead Map.empty

parentMapping :: [DependencyRep] -> DependencyParentAssoc
parentMapping file = walkInternal (reverse file) parentForHead Map.empty

walkInternal :: [DependencyRep] -> ([DependencyRep] -> Set DependencyRep) -> DependencyAssoc -> DependencyAssoc
walkInternal file @ (x:xs) finder acc
        | xs == [] = Map.insertWith Set.union cleanName Set.empty acc
        | otherwise = walkInternal xs finder $ Map.insertWith Set.union cleanName (finder file) acc
        where cleanName = stripDependency x
