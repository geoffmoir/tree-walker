import System.Environment
import System.IO
import Data.Map as Map
import Data.Set as Set
import Data.List as List
import Data.Tree as Tree
import qualified Data.Text as Text
import qualified Data.Text.IO as Text

import Dependency.Parser as Parser
import Dependency.Analyser as Analyser

data Direction = Up
               | Down
               deriving (Eq, Show)
type Lines = [DependencyRep]
type Args = [String]
type Action = Lines -> Args -> IO ()
data Operation = Op { operatesOnSingleDep :: Bool
                    , action :: Action
                    }

formatDependencies :: DependencyRep -> Maybe (Set DependencyRep) -> DependencyRep
formatDependencies title Nothing = title ++ "\n None"
formatDependencies title (Just deps) = title ++ "\n" ++ (unlines . Set.toList) deps

listDependency :: Action
listDependency lns args = do
    let (dep:_) = args
    putStr $ formatDependencies "Dependencies\n-----------" $ Map.lookup dep $ Parser.childMapping lns
    putStr "\n"
    putStr $ formatDependencies "Parents\n-------" $ Map.lookup dep $ Parser.parentMapping lns

searchDependencies :: SearchType -> Action
searchDependencies searchType lns args = do
    let (dep:_) = args
        results = Analyser.search searchType (Parser.childMapping lns) dep
    chosenDep <- chooseFrom results
    chosenOp <- chooseFrom $ List.map fst $ List.filter (\(_, operation) -> operatesOnSingleDep operation == True) dispatch
    case List.lookup chosenOp dispatch of
        Just operation -> action operation lns (chosenDep : [])
        Nothing -> error "Should never get here"

chooseFrom :: (Show a) => [a] -> IO a
chooseFrom choices = do
    putStrLn $ unlines $ zipWith (\i option -> show i ++ ": " ++ (show option)) [0..] choices
    putStr "Choice #: "
    hFlush stdout
    chosen <- getLine
    return (choices !! (read chosen))

followDependency :: Direction -> Action
followDependency direction lns args = do
    let (dep:_) = args
        mapping = if direction == Up then Parser.parentMapping lns else Parser.childMapping lns
    putStrLn $ Tree.drawTree $ Analyser.follow mapping dep

dispatch :: [(String, Operation)]
dispatch = [ ("list", Op True listDependency)
           , ("search", Op False $ searchDependencies FreeText)
           , ("followup", Op True $ followDependency Up)
           , ("followdown", Op True $ followDependency Down)
           , ("artifact", Op False $ searchDependencies Artifact)
           , ("group", Op False $ searchDependencies Group)
           , ("version", Op False $ searchDependencies Version)
           ]

main = do
        (file:command:args) <- getArgs
        file <- readFile file
        let lns = lines file
        case List.lookup command dispatch of
            Just operation -> action operation lns args
            Nothing -> putStrLn $ "Invalid command " ++ command

-- navigate terminal
-- ability to choose dep after search